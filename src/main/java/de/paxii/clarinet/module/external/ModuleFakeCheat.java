package de.paxii.clarinet.module.external;

import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.event.EventHandler;
import de.paxii.clarinet.event.events.game.IngameTickEvent;
import de.paxii.clarinet.module.Module;
import de.paxii.clarinet.module.ModuleCategory;
import de.paxii.clarinet.util.chat.Chat;
import de.paxii.clarinet.util.module.killaura.TimeManager;
import de.paxii.clarinet.util.player.PlayerUtils;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumHand;

/**
 * Created by Lars on 19.06.2016.
 */
public class ModuleFakeCheat extends Module {
  private String fakePlayerName;
  private TimeManager timeManager;

  public ModuleFakeCheat() {
    super("FakeCheat", ModuleCategory.COMBAT);

    this.setCommand(true);
    this.setSyntax("fakecheat <playername>");
    this.setDescription("Fakes usage of KillAura for the given Player. ");
    this.setVersion("1.1");
    this.setBuildVersion(17500);

    this.timeManager = new TimeManager();
  }

  @Override
  public void onEnable() {
    Wrapper.getEventManager().register(this);
  }

  @EventHandler
  public void onIngameTick(IngameTickEvent event) {
    EntityPlayer fakePlayer = PlayerUtils.getPlayerByName(this.fakePlayerName);

    if (fakePlayer != null) {
      double distance = fakePlayer.getDistanceToEntity(Wrapper.getPlayer());
      if (distance <= 4.5F) {
        float[] angles = this.getAngles(fakePlayer);

        fakePlayer.rotationYaw = angles[0];
        fakePlayer.rotationYawHead = angles[0];
        fakePlayer.rotationPitch = angles[1];


        if (timeManager.sleep(200)) {
          fakePlayer.swingArm(EnumHand.MAIN_HAND);
          this.timeManager.updateLast();
        }
      }
    }

    this.timeManager.updateTimer();
  }

  @Override
  public void onCommand(String[] args) {
    if (args.length >= 1) {
      this.fakePlayerName = args[0];

      Chat.printClientMessage(String.format("Set FakeCheat for Player %s.", this.fakePlayerName));
    } else {
      Chat.printClientMessage("Too few arguments!");
    }
  }

  private float[] getAngles(final EntityLivingBase entityLiving) {
    double difX = Wrapper.getPlayer().posX - entityLiving.posX;
    double difY = (Wrapper.getPlayer().posY + Wrapper.getPlayer().getEyeHeight()) - (entityLiving.posY + entityLiving.getEyeHeight());
    double difZ = Wrapper.getPlayer().posZ - entityLiving.posZ;
    double helper = Math.sqrt(difX * difX + difZ * difZ);
    float yaw = (float) (Math.atan2(difZ, difX) * 180 / Math.PI) - 90;
    float pitch = (float) -(Math.atan2(difY, helper) * 180 / Math.PI);
    return (new float[]{yaw, pitch});
  }

  @Override
  public void onDisable() {
    Wrapper.getEventManager().unregister(this);
  }
}
